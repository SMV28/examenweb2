from django.shortcuts import render

from .models import(Empleado,Actividad,Reporte)

from .forms import(AltaDeEmpleado,AltaDeActividad,AltaDeReporte)

def inicio_view(request):
    return render(request, 'inicio.html', {})


def empleados_view(request):
    empleados = list(Empleado.objects.all())

    context = {
        'empleados' : empleados
    }
    return render(request, 'empleados.html', context)

def actividades_view(request):
    actividades = list(Actividad.objects.all())

    context = {
        'actividades' : actividades
    }
    return render(request, 'actividades.html', context)



def empleados_registro_view(request):
    form = AltaDeEmpleado()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeEmpleado(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            apellido_paterno = form.cleaned_data['apellido_paterno']
            apellido_materno = form.cleaned_data['apellido_materno']
            puesto = form.cleaned_data['puesto']

            nuevo_empleado = Empleado(nombre=nombre, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, puesto=puesto)
            nuevo_empleado.save()

    return render(request, 'empleados-registro.html', context)

def actividades_registro_view(request):
    form = AltaDeActividad()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeActividad(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            descripcion = form.cleaned_data['descripcion']

            nueva_actividad = Actividad(nombre=nombre, descripcion=descripcion)
            nueva_actividad.save()
    return render(request, 'actividades-registro.html', context)

def reporte_registro_view(request):
    form = AltaDeReporte()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeReporte(request.POST)
        if form.is_valid():
            actividad = form.cleaned_data['actividad']
            empleado = form.cleaned_data['empleado']
            fecha_de_inicio = form.cleaned_data['fecha_de_inicio']
            fecha_de_finalizacion = form.cleaned_data['fecha_de_finalizacion']

            nueva_tarea = Reporte(actividad=actividad, empleado=empleado , fecha_de_inicio=fecha_de_inicio, fecha_de_finalizacion=fecha_de_finalizacion)
            nueva_tarea.save()
    return render(request, 'reporte-registro.html', context)


def reportes_view(request):
    empleados_existentes = list(Empleado.objects.all())

    desempeno_de_empleados = []
    tareas_de_empleados = []
    for empleado in empleados_existentes:
        cantidad_de_tareas = Reporte.objects.filter(empleado__id=empleado.id).count()
        desempeno_de_empleado = DesempenoDeEmpleado(empleado, cantidad_de_tareas)
        desempeno_de_empleados.append(desempeno_de_empleado)

        tareas_de_empleado_queryset = Reporte.objects.filter(empleado__id=empleado.id)
        tareas_de_empleado = TareasPorEmpleado(empleado, list(tareas_de_empleado_queryset))
        tareas_de_empleados.append(tareas_de_empleado)

    context = {
        'desempeno_de_empleados' : desempeno_de_empleados,
        'tareas_de_empleados' : tareas_de_empleados
    }
    return render(request, 'reportes.html', context)

class DesempenoDeEmpleado:
    def __init__(self, empleado, cantidad_de_tareas):
        self.empleado = empleado
        self.cantidad_de_tareas = cantidad_de_tareas

class TareasPorEmpleado:
    def __init__(self, empleado, tareas):
        self.empleado = empleado
        self.tareas = tareas


