from django.contrib import admin

from .models import Empleado, Actividad, Reporte

admin.site.register(Empleado)
admin.site.register(Actividad)
admin.site.register(Reporte)