from django import forms

from .models import (Empleado, Actividad, Reporte)

class AltaDeEmpleado(forms.Form):
    nombre = forms.CharField(max_length=30,label="Nombre",widget=forms.TextInput(attrs={'class': 'form-control'}))
    apellido_paterno = forms.CharField(max_length=30,label="Apellido Paterno",widget=forms.TextInput(attrs={'class': 'form-control'}))
    apellido_materno = forms.CharField(max_length=30,label="Apellido Materno",widget=forms.TextInput(attrs={'class': 'form-control'}))
    puesto = forms.CharField(max_length=30,label="puesto",widget=forms.TextInput(attrs={'class': 'form-control'}))

class AltaDeActividad(forms.Form):
    nombre = forms.CharField(max_length=30,label="Nombre",widget=forms.TextInput(attrs={'class': 'form-control'}))
    descripcion = forms.CharField(max_length=200,label="Descripcion",widget=forms.TextInput(attrs={'class': 'form-control'}))


class AltaDeReporte(forms.Form):
    actividad = forms.ModelChoiceField(queryset=Actividad.objects.all(),label="Actividades",widget=forms.Select(attrs={'class': 'form-control'}))
    empleado = forms.ModelChoiceField(queryset=Empleado.objects.all(),label="Empleados",widget=forms.Select(attrs={'class': 'form-control'}))
    fecha_de_inicio = forms.DateField(label="Fecha de Inicio",widget=forms.TextInput(attrs={'id':'datepickerOne','class': 'form-control','placeholder': 'Fecha de Inicio'}))
    fecha_de_finalizacion = forms.DateField(label="Fecha de Finalización",widget=forms.TextInput(attrs={'id':'datepickerTwo','class': 'form-control','placeholder': 'Fecha de Finalización'}))
