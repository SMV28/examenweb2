# Generated by Django 2.1.5 on 2019-03-09 11:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Actividad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=30)),
                ('descripcion', models.CharField(default='', max_length=200)),
            ],
            options={
                'verbose_name_plural': 'Actividades',
            },
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=30)),
                ('apellido_paterno', models.CharField(max_length=30)),
                ('apellido_materno', models.CharField(max_length=30)),
                ('puesto', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name_plural': 'Empleados',
            },
        ),
        migrations.CreateModel(
            name='Reporte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_de_inicio', models.DateField()),
                ('fecha_de_finalizacion', models.DateField()),
                ('actividad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.Actividad')),
                ('empleado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.Empleado')),
            ],
            options={
                'verbose_name_plural': 'Tareas',
            },
        ),
    ]
