from django.db import models

# Create your models here.
class Empleado(models.Model):
    nombre = models.CharField(max_length=30)
    apellido_paterno = models.CharField(max_length=30)
    apellido_materno = models.CharField(max_length=30)
    puesto = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "Empleados"

    def __str__(self):
        return "%s %s %s %s" % (self.nombre, self.apellido_paterno, self.apellido_materno, self.puesto)

class Actividad(models.Model):
    nombre = models.CharField(max_length=30)
    descripcion=models.CharField(max_length=200, default="")

    class Meta:
        verbose_name_plural = "Actividades"

    def __str__(self):
        return "%s" % (self.nombre)



class Reporte(models.Model):
    actividad = models.ForeignKey(Actividad, on_delete=models.CASCADE)
    empleado = models.ForeignKey(Empleado, on_delete=models.CASCADE)
    fecha_de_inicio = models.DateField()
    fecha_de_finalizacion = models.DateField()

    class Meta:
        verbose_name_plural = "Tareas"
