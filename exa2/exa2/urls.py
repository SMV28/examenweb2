"""exa2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from home import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.inicio_view, name='inicio'),
    path('empleados/', views.empleados_view, name='empleados'),
    path('actividades/', views.actividades_view, name='actividades'),
    path('reportes/', views.reportes_view, name='reportes'),
    path('registrarempleados/', views.empleados_registro_view, name='empleados_registro'),
    path('registraractividades/', views.actividades_registro_view, name='actividades_registro'),
    path('registrarreportes/', views.reporte_registro_view, name='reporte_registro'),
]